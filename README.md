# asking-price

거래소 별 호가 관련 모듈
- Upbit

## Versions

### 1.0.0
- UPBIT 지원
- `boolean check (exchange, market, price)`
- `boolean checkMin (exchange, market, price)`
- `boolean checkUnit (exchange, market, price)`
- `number getMin (exchange, market)`
- `number getUnit (exchange, market, price)`
- `number trim (exchange, market, price)`
