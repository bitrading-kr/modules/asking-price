"use strict";
/**
 * @fileOverview 호가 확인
 * @name askingPrice.js
 * @author https://bitrading.kr
 * @copyright https://bitrading.kr
 * @created 2018-11-02
 */

const units = require('./lib/units.js'),
      mins = require('./lib/mins.js');

const _ = require('lodash');

const VERSION = '1.0.1';

/**
 * @function calcFloat
 * @description float 계산
 *
 * @param {number} num 숫자
 *
 * @return {number} 소수점 3자리 반올림 값
 */
function calcFloat (num, point=3) {
  return parseFloat(num).toFixed(point);
}

class AskingPrice {
  get version () {
    return VERSION;
  }

  check (exchange, market, price) {
    return this.checkMin(exchange, market, price) && this.checkUnit(exchange, market, price);
  }

  checkMin (exchange, market, price) { // min 이상인지 체크
    return price >= this.getMin(exchange, market);
  }

  checkUnit (exchange, market, price) {
    let unit = this.getUnit(exchange, market, price);
    return calcFloat(calcFloat(price/unit, 10)%1, 10) == 0;
  }

  getMin (exchange, market) {
    let func = _.get(mins, [exchange, market]);
    if (!_.isFunction(func))
      throw new Error(`[${exchange}, ${market}] 최저가를 지원하지 않습니다.`);
    return func();
  }

  getUnit (exchange, market, price) {
    let func = _.get(units, [exchange, market]);
    if (!_.isFunction(func))
      throw new Error(`[${exchange}, ${market}] 최저가를 지원하지 않습니다.`);
    return func(price);
  }

  trim (exchange, market, price) {
    let unit = this.getUnit(exchange, market, price),
        point = Math.log10(unit)*-1;

    if (point <= 0) { // unit이 10^0
      return parseFloat(calcFloat(Math.floor(calcFloat(price/unit, 3))*unit, 3));
    } else { // unit이 소수점
      return parseFloat(calcFloat(Math.floor(calcFloat(price/unit, point))*unit, point));
    }
  }
}

module.exports = new AskingPrice();
