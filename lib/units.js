module.exports = {
  UPBIT: {
    KRW: (price) => {
      if (price < 10) { // ~ 9.99
        return 0.01;
      } else if (price < 100) { // 10 ~ 99.9
        return 0.1;
      } else if (price < 1000) { // 100 ~ 995
        return 1;
      } else if (price < 10000) { // 1000 ~ 9990
        return 5;
      } else if (price < 100000) { // 10000 ~ 99990
        return 10;
      } else if (price < 500000) { // 100000 ~ 499900
        return 50;
      } else if (price < 1000000) { // 500000 ~ 999950
        return 100;
      } else if (price < 2000000) { // 1000000 ~ 1999900
        return 500;
      } else {
        return 1000;
      }
    },
    BTC: (price) => {
      return 0.00000001;
    },
    ETH: (price) => {
      return 0.00000001;
    },
    USDT: (price) => {
      return 0.001;
    }
  }
};
