module.exports = {
  UPBIT: {
    KRW: () => {
      return 500;
    },
    BTC: () => {
      return 0.0005;
    },
    ETH: () => {
      return 0.0005;
    },
    USDT: () => {
      return 0.0005;
    }
  }
};
