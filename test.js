const ap = require('./index.js');
const assert = require('assert');

describe(`UPBIT`, function () {
  describe(`#.min()`, function () {
    it(`KRW`, function () {
      assert.equal(ap.getMin('UPBIT', 'KRW'), 500);
    });
    it(`BTC`, function () {
      assert.equal(ap.getMin('UPBIT', 'BTC'), 0.0005);
    });
    it(`ETH`, function () {
      assert.equal(ap.getMin('UPBIT', 'ETH'), 0.0005);
    });
    it(`USDT`, function () {
      assert.equal(ap.getMin('UPBIT', 'USDT'), 0.0005);
    });
  });

  describe(`#.checkUnit()`, function () {
    describe(`#.KRW()`, function () {
      it(`>= 2000000`, function () {
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000000), true);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000001), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000010), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000100), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000500), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2001000), true);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2010000), true);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2100000), true);
      });
      it(`>= 1000000`, function () {
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1999999), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1999500), true);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1111111), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1111100), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1000000), true);
      });
      it(`>= 500000`, function () {
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 999999), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 999900), true);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 999000), true);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 555555), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 555550), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 555500), true);
      });
      it(`>= 100000`, function () {
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 499999), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 499950), true);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 100050), true);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 100005), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 100000), true);
      });
      it(`>= 2000000.123`, function () {
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000000.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000001.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000010.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000100.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2000500.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2001000.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2010000.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 2100000.123), false);
      });
      it(`>= 1000000.123`, function () {
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1999999.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1999500.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1111111.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1111100.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 1000000.123), false);
      });
      it(`>= 500000.123`, function () {
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 999999.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 999900.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 999000.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 555555.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 555550.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 555500.123), false);
      });
      it(`>= 100000.123`, function () {
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 499999.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 499950.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 100050.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 100005.123), false);
        assert.equal(ap.checkUnit('UPBIT', 'KRW', 100000.123), false);
      });
    });
  });

	describe(`#.trim()`, function () {
    describe(`#.KRW()`, function () {
      it(`>= 2000000`, function () {
        assert.equal(ap.trim('UPBIT', 'KRW', 2000000), 2000000);
        assert.equal(ap.trim('UPBIT', 'KRW', 2342344), 2342000);
        assert.equal(ap.trim('UPBIT', 'KRW', 98989899), 98989000);
        assert.equal(ap.trim('UPBIT', 'KRW', 3434344), 3434000);

        assert.equal(ap.trim('UPBIT', 'KRW', 2000000.234324324), 2000000);
        assert.equal(ap.trim('UPBIT', 'KRW', 2342344.523525), 2342000);
        assert.equal(ap.trim('UPBIT', 'KRW', 98989899.234234), 98989000);
        assert.equal(ap.trim('UPBIT', 'KRW', 3434344.234234), 3434000);
      });
      it(`>= 1000000`, function () {
        assert.equal(ap.trim('UPBIT', 'KRW', 1000000), 1000000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1342344), 1342000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1889899), 1889500);
        assert.equal(ap.trim('UPBIT', 'KRW', 1434344), 1434000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1999999), 1999500);
        assert.equal(ap.trim('UPBIT', 'KRW', 1500000), 1500000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1050000), 1050000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1005000), 1005000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1000500), 1000500);
        assert.equal(ap.trim('UPBIT', 'KRW', 1000050), 1000000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1000005), 1000000);

        assert.equal(ap.trim('UPBIT', 'KRW', 1000000.234324), 1000000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1342344.3535), 1342000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1889899.23423), 1889500);
        assert.equal(ap.trim('UPBIT', 'KRW', 1434344.343), 1434000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1999999.3242), 1999500);
        assert.equal(ap.trim('UPBIT', 'KRW', 1500000.234), 1500000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1050000.345345), 1050000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1005000.346346), 1005000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1000500.23432), 1000500);
        assert.equal(ap.trim('UPBIT', 'KRW', 1000050.234), 1000000);
        assert.equal(ap.trim('UPBIT', 'KRW', 1000005.6457), 1000000);

      });
      it(`>= 500000`, function () {
        assert.equal(ap.trim('UPBIT', 'KRW', 999999), 999900);
        assert.equal(ap.trim('UPBIT', 'KRW', 999990), 999900);
        assert.equal(ap.trim('UPBIT', 'KRW', 999900), 999900);
        assert.equal(ap.trim('UPBIT', 'KRW', 500000), 500000);
        assert.equal(ap.trim('UPBIT', 'KRW', 500005), 500000);
        assert.equal(ap.trim('UPBIT', 'KRW', 500050), 500000);
        assert.equal(ap.trim('UPBIT', 'KRW', 500500), 500500);
        assert.equal(ap.trim('UPBIT', 'KRW', 505000), 505000);

        assert.equal(ap.trim('UPBIT', 'KRW', 999999.23432), 999900);
        assert.equal(ap.trim('UPBIT', 'KRW', 999990.234), 999900);
        assert.equal(ap.trim('UPBIT', 'KRW', 999900.5423), 999900);
        assert.equal(ap.trim('UPBIT', 'KRW', 500000.345), 500000);
        assert.equal(ap.trim('UPBIT', 'KRW', 500005.346), 500000);
        assert.equal(ap.trim('UPBIT', 'KRW', 500050.989898989), 500000);
        assert.equal(ap.trim('UPBIT', 'KRW', 500500.34543534), 500500);
        assert.equal(ap.trim('UPBIT', 'KRW', 505000.568456857), 505000);
      });
      it(`>= 100000`, function () {
        assert.equal(ap.trim('UPBIT', 'KRW', 499999), 499950);
        assert.equal(ap.trim('UPBIT', 'KRW', 499990), 499950);
        assert.equal(ap.trim('UPBIT', 'KRW', 499900), 499900);
        assert.equal(ap.trim('UPBIT', 'KRW', 499000), 499000);
        assert.equal(ap.trim('UPBIT', 'KRW', 490000), 490000);
        assert.equal(ap.trim('UPBIT', 'KRW', 199900), 199900);
        assert.equal(ap.trim('UPBIT', 'KRW', 199990), 199950);
        assert.equal(ap.trim('UPBIT', 'KRW', 199999), 199950);
        assert.equal(ap.trim('UPBIT', 'KRW', 100000), 100000);

        assert.equal(ap.trim('UPBIT', 'KRW', 499999.234324), 499950);
        assert.equal(ap.trim('UPBIT', 'KRW', 499990.234), 499950);
        assert.equal(ap.trim('UPBIT', 'KRW', 499900.234), 499900);
        assert.equal(ap.trim('UPBIT', 'KRW', 499000.333), 499000);
      });
      it(`>= 10000`, function () {
        assert.equal(ap.trim('UPBIT', 'KRW', 99999), 99990);
        assert.equal(ap.trim('UPBIT', 'KRW', 99990), 99990);
        assert.equal(ap.trim('UPBIT', 'KRW', 99900), 99900);
        assert.equal(ap.trim('UPBIT', 'KRW', 99000), 99000);
        assert.equal(ap.trim('UPBIT', 'KRW', 49999), 49990);
        assert.equal(ap.trim('UPBIT', 'KRW', 49990), 49990);
        assert.equal(ap.trim('UPBIT', 'KRW', 49900), 49900);
        assert.equal(ap.trim('UPBIT', 'KRW', 49000), 49000);
        assert.equal(ap.trim('UPBIT', 'KRW', 19999), 19990);
        assert.equal(ap.trim('UPBIT', 'KRW', 19990), 19990);
        assert.equal(ap.trim('UPBIT', 'KRW', 10000), 10000);

        assert.equal(ap.trim('UPBIT', 'KRW', 99999.234234), 99990);
        assert.equal(ap.trim('UPBIT', 'KRW', 99990.3432), 99990);
        assert.equal(ap.trim('UPBIT', 'KRW', 99900.64765485), 99900);
        assert.equal(ap.trim('UPBIT', 'KRW', 99000.34543), 99000);
        assert.equal(ap.trim('UPBIT', 'KRW', 49999.45654), 49990);
        assert.equal(ap.trim('UPBIT', 'KRW', 49990.345345), 49990);
        assert.equal(ap.trim('UPBIT', 'KRW', 49900.34534), 49900);
        assert.equal(ap.trim('UPBIT', 'KRW', 49000.3267327), 49000);
        assert.equal(ap.trim('UPBIT', 'KRW', 19999.346), 19990);
        assert.equal(ap.trim('UPBIT', 'KRW', 19990.34534), 19990);
        assert.equal(ap.trim('UPBIT', 'KRW', 10000.345345), 10000);
      });
      it(`>= 1000`, function () {
        assert.equal(ap.trim('UPBIT', 'KRW', 9999), 9995);
        assert.equal(ap.trim('UPBIT', 'KRW', 9999), 9995);
        assert.equal(ap.trim('UPBIT', 'KRW', 9990), 9990);
        assert.equal(ap.trim('UPBIT', 'KRW', 9900), 9900);
        assert.equal(ap.trim('UPBIT', 'KRW', 4999), 4995);
        assert.equal(ap.trim('UPBIT', 'KRW', 4999), 4995);
        assert.equal(ap.trim('UPBIT', 'KRW', 4990), 4990);
        assert.equal(ap.trim('UPBIT', 'KRW', 4900), 4900);
        assert.equal(ap.trim('UPBIT', 'KRW', 1999), 1995);
        assert.equal(ap.trim('UPBIT', 'KRW', 1999), 1995);
        assert.equal(ap.trim('UPBIT', 'KRW', 1000), 1000);

        assert.equal(ap.trim('UPBIT', 'KRW', 9999.343), 9995);
        assert.equal(ap.trim('UPBIT', 'KRW', 9999.647), 9995);
        assert.equal(ap.trim('UPBIT', 'KRW', 9990.345), 9990);
        assert.equal(ap.trim('UPBIT', 'KRW', 9900.456), 9900);
        assert.equal(ap.trim('UPBIT', 'KRW', 4999.345), 4995);
        assert.equal(ap.trim('UPBIT', 'KRW', 4999.345), 4995);
        assert.equal(ap.trim('UPBIT', 'KRW', 4990.326), 4990);
        assert.equal(ap.trim('UPBIT', 'KRW', 4900.346), 4900);
        assert.equal(ap.trim('UPBIT', 'KRW', 1999.345), 1995);
        assert.equal(ap.trim('UPBIT', 'KRW', 1999.345), 1995);
        assert.equal(ap.trim('UPBIT', 'KRW', 1000.99999999999999999999), 1000);

      });
      it(`>= 100`, function () {
        assert.equal(ap.trim('UPBIT', 'KRW', 999.9), 999);
        assert.equal(ap.trim('UPBIT', 'KRW', 999.5), 999);
        assert.equal(ap.trim('UPBIT', 'KRW', 999.1), 999);
        assert.equal(ap.trim('UPBIT', 'KRW', 990.9), 990);
        assert.equal(ap.trim('UPBIT', 'KRW', 499.23423432432), 499);
        assert.equal(ap.trim('UPBIT', 'KRW', 499.234325325236436), 499);
        assert.equal(ap.trim('UPBIT', 'KRW', 499.234932804), 499);
        assert.equal(ap.trim('UPBIT', 'KRW', 490), 490);
        assert.equal(ap.trim('UPBIT', 'KRW', 199), 199);
        assert.equal(ap.trim('UPBIT', 'KRW', 199.23423432), 199);
        assert.equal(ap.trim('UPBIT', 'KRW', 100), 100);
      });
      it(`< 100`, function () {
        assert.equal(ap.trim('UPBIT', 'KRW', 99.3), 99.3);
        assert.equal(ap.trim('UPBIT', 'KRW', 99.43434), 99.4);
        assert.equal(ap.trim('UPBIT', 'KRW', 99.234234), 99.2);
        assert.equal(ap.trim('UPBIT', 'KRW', 99.9), 99.9);
        assert.equal(ap.trim('UPBIT', 'KRW', 49), 49);
        assert.equal(ap.trim('UPBIT', 'KRW', 49), 49);
        assert.equal(ap.trim('UPBIT', 'KRW', 49), 49);
        assert.equal(ap.trim('UPBIT', 'KRW', 49), 49);
        assert.equal(ap.trim('UPBIT', 'KRW', 19), 19);
        assert.equal(ap.trim('UPBIT', 'KRW', 19), 19);
        assert.equal(ap.trim('UPBIT', 'KRW', 10), 10);
        assert.equal(ap.trim('UPBIT', 'KRW', 13.25), 13.2);
      });
    });
    describe(`#.BTC()`, function () {
      it(`9`, function () {
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000000009), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000000009), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000000009), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000009), 0.00000009);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000009), 0.0000009);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000009), 0.000009);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00009), 0.00009);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0009), 0.0009);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.009), 0.009);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.09), 0.09);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.9), 0.9);
        assert.equal(ap.trim('UPBIT', 'BTC', 9), 9);
        assert.equal(ap.trim('UPBIT', 'BTC', 90), 90);
      });
      it(`5`, function () {
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000000005), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000000005), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000000005), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000005), 0.00000005);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000005), 0.0000005);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000005), 0.000005);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00005), 0.00005);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0005), 0.0005);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.005), 0.005);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.05), 0.05);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.5), 0.5);
        assert.equal(ap.trim('UPBIT', 'BTC', 5), 5);
        assert.equal(ap.trim('UPBIT', 'BTC', 50), 50);
      });
      it(`1`, function () {
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000000001), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000000001), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000000001), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000001), 0.00000001);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000001), 0.0000001);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000001), 0.000001);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00001), 0.00001);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0001), 0.0001);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.001), 0.001);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.01), 0.01);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.1), 0.1);
        assert.equal(ap.trim('UPBIT', 'BTC', 1), 1);
        assert.equal(ap.trim('UPBIT', 'BTC', 10), 10);
      });
      it(`9.99...`, function () {
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000000009), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000000009), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000000009), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000009), 0.00000009);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000099999), 0.00000099);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000999999), 0.00000999);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00009999999), 0.00009999);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00099999999), 0.00099999);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00999999999), 0.00999999);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.09999999999), 0.09999999);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.99999999999), 0.99999999);
        assert.equal(ap.trim('UPBIT', 'BTC', 9.9999999999), 9.99999999);
        assert.equal(ap.trim('UPBIT', 'BTC', 90.9999999999), 90.99999999);
      });
      it(`5.55...`, function () {
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000000005), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000000005), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000000005), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000005), 0.00000005);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000055555), 0.00000055);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000555555), 0.00000555);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00005555555), 0.00005555);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00055555555), 0.00055555);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00555555555), 0.00555555);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.05555555555), 0.05555555);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.55555555555), 0.55555555);
        assert.equal(ap.trim('UPBIT', 'BTC', 5.5555555555), 5.55555555);
        assert.equal(ap.trim('UPBIT', 'BTC', 50.5555555555), 50.55555555);
      });
      it(`1.11...`, function () {
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000000001), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.0000000001), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.000000001), 0);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000001), 0.00000001);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000011111), 0.00000011);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00000111111), 0.00000111);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00001111111), 0.00001111);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00011111111), 0.00011111);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.00111111111), 0.00111111);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.01111111111), 0.01111111);
        assert.equal(ap.trim('UPBIT', 'BTC', 0.11111111111), 0.11111111);
        assert.equal(ap.trim('UPBIT', 'BTC', 1.1111111111), 1.11111111);
        assert.equal(ap.trim('UPBIT', 'BTC', 10.1111111111), 10.11111111);
      });
    });
    describe(`#.ETH()`, function () {
      it(`9`, function () {
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000000009), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000000009), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000000009), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000009), 0.00000009);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000009), 0.0000009);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000009), 0.000009);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00009), 0.00009);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0009), 0.0009);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.009), 0.009);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.09), 0.09);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.9), 0.9);
        assert.equal(ap.trim('UPBIT', 'ETH', 9), 9);
        assert.equal(ap.trim('UPBIT', 'ETH', 90), 90);
      });
      it(`5`, function () {
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000000005), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000000005), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000000005), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000005), 0.00000005);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000005), 0.0000005);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000005), 0.000005);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00005), 0.00005);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0005), 0.0005);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.005), 0.005);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.05), 0.05);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.5), 0.5);
        assert.equal(ap.trim('UPBIT', 'ETH', 5), 5);
        assert.equal(ap.trim('UPBIT', 'ETH', 50), 50);
      });
      it(`1`, function () {
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000000001), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000000001), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000000001), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000001), 0.00000001);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000001), 0.0000001);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000001), 0.000001);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00001), 0.00001);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0001), 0.0001);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.001), 0.001);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.01), 0.01);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.1), 0.1);
        assert.equal(ap.trim('UPBIT', 'ETH', 1), 1);
        assert.equal(ap.trim('UPBIT', 'ETH', 10), 10);
      });
      it(`9.99...`, function () {
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000000009), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000000009), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000000009), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000009), 0.00000009);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000099999), 0.00000099);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000999999), 0.00000999);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00009999999), 0.00009999);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00099999999), 0.00099999);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00999999999), 0.00999999);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.09999999999), 0.09999999);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.99999999999), 0.99999999);
        assert.equal(ap.trim('UPBIT', 'ETH', 9.9999999999), 9.99999999);
        assert.equal(ap.trim('UPBIT', 'ETH', 90.9999999999), 90.99999999);
      });
      it(`5.55...`, function () {
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000000005), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000000005), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000000005), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000005), 0.00000005);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000055555), 0.00000055);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000555555), 0.00000555);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00005555555), 0.00005555);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00055555555), 0.00055555);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00555555555), 0.00555555);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.05555555555), 0.05555555);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.55555555555), 0.55555555);
        assert.equal(ap.trim('UPBIT', 'ETH', 5.5555555555), 5.55555555);
        assert.equal(ap.trim('UPBIT', 'ETH', 50.5555555555), 50.55555555);
      });
      it(`1.11...`, function () {
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000000001), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.0000000001), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.000000001), 0);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000001), 0.00000001);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000011111), 0.00000011);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00000111111), 0.00000111);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00001111111), 0.00001111);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00011111111), 0.00011111);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.00111111111), 0.00111111);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.01111111111), 0.01111111);
        assert.equal(ap.trim('UPBIT', 'ETH', 0.11111111111), 0.11111111);
        assert.equal(ap.trim('UPBIT', 'ETH', 1.1111111111), 1.11111111);
        assert.equal(ap.trim('UPBIT', 'ETH', 10.1111111111), 10.11111111);
      });
    });

    describe(`#.USDT()`, function () {
      it(`9`, function () {
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0000000009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.000000009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0000009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.000009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.009), 0.009);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.09), 0.09);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.9), 0.9);
        assert.equal(ap.trim('UPBIT', 'USDT', 9), 9);
        assert.equal(ap.trim('UPBIT', 'USDT', 90), 90);
      });
      it(`5`, function () {
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0000000005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.000000005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0000005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.000005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.005), 0.005);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.05), 0.05);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.5), 0.5);
        assert.equal(ap.trim('UPBIT', 'USDT', 5), 5);
        assert.equal(ap.trim('UPBIT', 'USDT', 50), 50);
      });
      it(`1`, function () {
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0000000001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.000000001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0000001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.000001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.0001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.001), 0.001);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.01), 0.01);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.1), 0.1);
        assert.equal(ap.trim('UPBIT', 'USDT', 1), 1);
        assert.equal(ap.trim('UPBIT', 'USDT', 10), 10);
      });
      it(`9.99..`, function () {
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000009), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000099), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000999), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000009999), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000099999), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000999999), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00009999999), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00099999999), 0.001);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00999999999), 0.010);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.09999999999), 0.100);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.99999999999), 1.000);
        assert.equal(ap.trim('UPBIT', 'USDT', 9.99999999999), 10.000);
        assert.equal(ap.trim('UPBIT', 'USDT', 90.999), 90.999);
      });
      it(`5.55..`, function () {
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000005), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000055), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000555), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000005555), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000055555), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000555555), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00005555555), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00055555555), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00555555555), 0.005);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.05555555555), 0.055);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.55555555555), 0.555);
        assert.equal(ap.trim('UPBIT', 'USDT', 5.55555555555), 5.555);
        assert.equal(ap.trim('UPBIT', 'USDT', 50.555), 50.555);
      });
      it(`1.11..`, function () {
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000001), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000011), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000000111), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000001111), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000011111), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00000111111), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00001111111), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00011111111), 0);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.00111111111), 0.001);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.01111111111), 0.011);
        assert.equal(ap.trim('UPBIT', 'USDT', 0.11111111111), 0.111);
        assert.equal(ap.trim('UPBIT', 'USDT', 1.11111111111), 1.111);
        assert.equal(ap.trim('UPBIT', 'USDT', 10.111), 10.111);
      });
    });
  });
});
